<?php

for ($i = 1; $i <= 15; $i++) {
    if ($i % 3 == 0 && $i % 5 == 0) {
        print("FizzBuzz");
    } elseif ($i % 5 == 0) {
        print("Buzz");
    } elseif ($i % 3 == 0) {
        print("Fizz");
    } else {
        print($i);
    }
}
