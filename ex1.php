<?php

$numbers = [1, 2, '3', 6, 2, 3, 2, 3];
$result = 0;

foreach ($numbers as $num) {
    if ($num === 3) {
        $result++;
    } else {
        continue;
    }
}

print("found it $result times");
