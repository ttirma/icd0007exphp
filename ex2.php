<?php

$numbers = [1, 2, '3', 6, 2, 3, 2, 3];

function isInList($list, $elementToBeFound) {
    foreach ($list as $num) {
        if ($elementToBeFound === $num) {
            return true;
        }
    }
    return false;
}
