<?php

$numbers = [1, 2, 5, 6, 2, 11, 2, 7];

function getOddNumbers($list) {
    foreach ($list as $num) {
        if ($num % 2 != 0) {
            unset($list[array_search($num, $list)]);
        }
    }
    return $list;
}
